from .commands import *
from typing import List


class ShellVisitor(CommandVisitor):
    def __init__(self):
        self._lines = []

    def visitExec(self, cmd: ExecCommand):
        shell_cmd = ' '.join(cmd.args)
        if cmd.register:
            self._lines.append("{:s}=$({:s})".format(cmd.register, shell_cmd))
        else:
            self._lines.append(shell_cmd)

    def visitCopy(self, cmd: CopyCommand):
        self._lines.append('cp -r "/install/{:s}" "{:s}"'.format(cmd.src, cmd.dest))

    def shell_script(self) -> str:
        return '\n'.join(['#!/bin/sh -e']+self._lines)


class PackageVisitor(CommandVisitor):
    def __init__(self):
        self._packages = []

    def visitPackage(self, cmd: PackageCommand) -> None:
        self._packages += cmd.items

    def packages(self) -> List[str]:
        return self._packages
