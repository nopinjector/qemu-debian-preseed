import os.path
import yaml
from typing import Any
from .. import schema
from .commands import (
    CommandVisitor,
    CopyCommand,
    ExecCommand,
    PackageCommand
)
from .shell import (
    ShellVisitor,
    PackageVisitor
)
from .tarfile import (
    TarVisitor,
    TarBuilder
)


class FeatureCommandList:
    _schema = schema.List(schema.Union(
        CopyCommand,
        ExecCommand,
        # LocalCommand,
        # PrintCommand,
        PackageCommand
    ))

    def __init__(self, obj):
        self._commands = obj

    @staticmethod
    def create_from_yaml(yaml_obj):
        schema_parser = schema.ParserManager()
        if not schema_parser.verify(FeatureCommandList._schema, yaml_obj):
            raise Exception('invalid feature file')

        result = schema_parser.parse(FeatureCommandList._schema, yaml_obj)
        return FeatureCommandList(result)

    def apply_visitor(self, visitor: CommandVisitor):
        for command in self._commands:
            command.visit(visitor)

    def verify(self):
        raise NotImplementedError


class FeatureFile:
    def __init__(self, path: str):
        yaml_obj = FeatureFile._read_feature_file(path)
        self._commands = FeatureCommandList.create_from_yaml(yaml_obj)
        self._path = path

    def create_tftp_content(self, tftp_dir: str):
        feature_basename = os.path.basename(self._path)
        tar_path = os.path.join(tftp_dir, '{:s}.tar.gz'.format(feature_basename))
        feature_file_base_path = os.path.dirname(os.path.abspath(self._path))

        tar_visitor = TarVisitor()
        self._commands.apply_visitor(tar_visitor)
        tarred_files = tar_visitor.paths()

        run_script = self._generate_shell_script()

        with TarBuilder(tar_path) as tar_builder:
            tar_builder.add_file('run.sh', run_script.encode('ascii'))
            for path in tarred_files:
                full_path = os.path.join(feature_file_base_path, path)
                tar_builder.add_path(full_path, feature_file_base_path)

    def get_preseed_commands(self):
        feature_basename = os.path.basename(self._path)
        tar_path = '{:s}.tar.gz'.format(feature_basename)

        return [
            'mkdir /target/install',
            'tftp -g -l /target/{0:s} -r {0:s} 172.16.0.2'.format(tar_path),
            'tar xzf /target/{0:s} -C /target/install/'.format(tar_path),
            'in-target /install/run.sh',
            'rm -rf /target/install /target/{0:s}'.format(tar_path)
        ]

    def get_preseed_packages(self):
        package_visitor = PackageVisitor()
        self._commands.apply_visitor(package_visitor)
        return package_visitor.packages()

    @staticmethod
    def _read_feature_file(path: str) -> Any:
        with open(path, 'rb') as fd:
            return yaml.safe_load(fd)

    def _generate_shell_script(self):
        shell_visitor = ShellVisitor()
        self._commands.apply_visitor(shell_visitor)
        return shell_visitor.shell_script()
