from .. import schema
from typing import Iterable, Type


class CommandVisitor:
    def visitExec(self, cmd) -> None:
        pass

    def visitCopy(self, cmd) -> None:
        pass

    def visitPackage(self, cmd) -> None:
        pass


class Command(schema.Structure):
    _keyword = NotImplemented

    @classmethod
    def transformations(cls) -> Iterable[Type[schema.Transformation]]:
        return [
            schema.KeywordUnpackTransformation,
            schema.FreeFormTransformation
        ]

    def visit(self, visitor: CommandVisitor):
        raise NotImplementedError


class CopyCommand(Command):
    _keyword = 'copy'
    src = schema.String()
    dest = schema.String()

    def visit(self, visitor: CommandVisitor) -> None:
        visitor.visitCopy(self)


class ExecCommand(Command):
    _keyword = 'command'
    _free_field = 'args'
    args = schema.List(schema.String())
    register = schema.Optional(schema.String())

    def visit(self, visitor: CommandVisitor) -> None:
        visitor.visitExec(self)


class PackageCommand(Command):
    _keyword = 'packages'
    _free_field = 'items'
    items = schema.List(schema.String())

    def visit(self, visitor: CommandVisitor) -> None:
        visitor.visitPackage(self)
