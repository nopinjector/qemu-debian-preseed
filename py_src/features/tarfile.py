import io
import tarfile
from .commands import *
from typing import Callable, List


class TarVisitor(CommandVisitor):
    def __init__(self):
        self._paths = []

    def visitCopy(self, cmd: CopyCommand):
        self._paths.append(cmd.src)

    def paths(self) -> List[str]:
        return self._paths


class TarBuilder:
    def __init__(self, path: str):
        self._tar = tarfile.open(path, 'x:gz')

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.finish()

    def add_path(self, path, base_path):
        self._tar.add(path, recursive=True, filter=TarBuilder._modify_tarinfo(base_path))

    def add_file(self, name, content):
        self._tar.addfile(TarBuilder._create_file_tarinfo(name, len(content)), io.BytesIO(content))

    def finish(self):
        self._tar.close()

    @staticmethod
    def _create_file_tarinfo(path: str, size: int):
        tarinfo = tarfile.TarInfo()
        tarinfo.name = path
        tarinfo.mode = 0o0755
        tarinfo.uid = tarinfo.gid = 0
        tarinfo.uname = tarinfo.gname = 'root'
        tarinfo.type = tarfile.REGTYPE
        tarinfo.size = size
        return tarinfo

    @staticmethod
    def _modify_tarinfo(path_prefix: str) -> Callable[[tarfile.TarInfo], tarfile.TarInfo]:
        def _filter(tarinfo: tarfile.TarInfo) -> tarfile.TarInfo:
            # Adjust owner and mode
            tarinfo.uid = tarinfo.gid = 0
            tarinfo.uname = tarinfo.gname = 'root'
            tarinfo.mode = 0o0755

            # Strip path hierarchy prefix
            tarinfo.name = tarinfo.name.replace('{:s}/'.format(path_prefix[1:]), '')

            return tarinfo

        return _filter
