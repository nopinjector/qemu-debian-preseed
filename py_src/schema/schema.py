import inspect
from typing import Any, Optional, Union, Type, List, Dict, Tuple, Callable
from .types import (
    SchemaType,
    TypeInteger,
    TypeString,
    TypeList,
    TypeOptional,
    TypeUnion,
    TypeStructure
)
from .exception import (
    SchemaException,
    SchemaParserException,
    SchemaVerificationException
)


class VerifyDelegate:
    def verify(self, ty: Type[SchemaType], obj: Any) -> bool:
        raise NotImplementedError


class ParserDelegate(VerifyDelegate):
    def parse(self, ty: Type[SchemaType], obj: Any) -> Any:
        raise NotImplementedError


class UnionTypeParser:
    ty = TypeUnion

    @staticmethod
    def verify(ty: Type[TypeUnion], value: Any, delegate: VerifyDelegate) -> bool:
        return any([delegate.verify(union_ty, value) for union_ty in ty.get_union_types()])

    @staticmethod
    def parse(ty: Type[TypeUnion], obj: Any, delegate: ParserDelegate) -> Any:
        for union_ty in ty.get_union_types():
            # We have to check which type of the union is able to handle the object
            if delegate.verify(union_ty, obj):
                return delegate.parse(union_ty, obj)

        raise SchemaParserException('unable to parse union type. no type can handle this object!')


class OptionalTypeParser:
    ty = TypeOptional

    @staticmethod
    def verify(ty: Type[TypeOptional], value: Any, delegate: VerifyDelegate) -> bool:
        if value is None:
            return True

        sub_ty = ty.get_sub_type()
        return delegate.verify(sub_ty, value)

    @staticmethod
    def parse(ty: Type[TypeOptional], obj: Any, delegate: ParserDelegate) -> Any:
        sub_ty = ty.get_sub_type()
        return delegate.parse(sub_ty, obj)


class ListTypeParser:
    ty = TypeList

    @staticmethod
    def verify(ty: Type[TypeList], value: Any, delegate: VerifyDelegate) -> bool:
        if not isinstance(value, list):
            return False

        list_ty = ty.get_sub_type()
        return all([delegate.verify(list_ty, item) for item in value])

    @staticmethod
    def parse(ty: Type[TypeList], obj: Any, delegate: ParserDelegate) -> list:
        list_ty = ty.get_sub_type()
        return [delegate.parse(list_ty, item) for item in obj]


class StringTypeParser:
    ty = TypeString

    @staticmethod
    def verify(ty: Type[TypeString], obj: Any, delegate: VerifyDelegate) -> bool:
        return isinstance(obj, str)

    @staticmethod
    def parse(ty: Type[TypeString], obj: Any, delegate: ParserDelegate) -> str:
        return obj


class IntegerTypeParser:
    ty = TypeInteger

    @staticmethod
    def verify(ty: Type[TypeInteger], obj: Any, delegate: VerifyDelegate) -> bool:
        return isinstance(obj, int)

    @staticmethod
    def parse(ty: Type[TypeInteger], obj: Any, delegate: ParserDelegate) -> int:
        return obj


class StructureTypeParser:
    ty = lambda x: issubclass(x, TypeStructure)

    @staticmethod
    def _apply_transformations(ty: Type[TypeStructure], obj: Any) -> Optional[Any]:
        for tr in ty.transformations():
            if not tr.transformable(ty, obj):
                return None

            obj = tr.transform(ty, obj)

        return obj

    @staticmethod
    def verify(ty: Type[TypeStructure], obj: Any, delegate: ParserDelegate) -> bool:
        obj = StructureTypeParser._apply_transformations(ty, obj)
        if obj is None or not isinstance(obj, dict):
            return False

        cls_attributes = ty.attributes()

        for cls_attr_name in cls_attributes:
            cls_attr = getattr(ty, cls_attr_name)
            obj_val = obj.get(cls_attr_name, None)

            if not delegate.verify(cls_attr, obj_val):
                return False

        return True

    @staticmethod
    def parse(ty: Type[TypeStructure], obj: Any, delegate: ParserDelegate) -> Any:
        obj = StructureTypeParser._apply_transformations(ty, obj)
        cls_attributes = ty.attributes()

        ty_obj = ty()
        for cls_attr_name in cls_attributes:
            cls_attr = getattr(ty, cls_attr_name)
            obj_val = obj.get(cls_attr_name, None)

            result = delegate.parse(cls_attr, obj_val)
            setattr(ty_obj, cls_attr_name, result)

        return ty_obj


class ParserManager(ParserDelegate):
    _parsers = [
        UnionTypeParser,
        OptionalTypeParser,
        ListTypeParser,
        StringTypeParser,
        IntegerTypeParser,
        StructureTypeParser,
    ]

    @staticmethod
    def _get_parser(ty: Type):
        for parser in ParserManager._parsers:
            parser_ty = parser.ty

            if inspect.isclass(parser_ty):
                if issubclass(ty, parser_ty):
                    return parser
            elif callable(parser_ty):
                if parser_ty(ty):
                    return parser

        raise SchemaVerificationException("no parser found!")

    def verify(self, ty: Type, obj: Any) -> bool:
        parser = ParserManager._get_parser(ty)
        return parser.verify(ty, obj, self)

    def parse(self, ty: Type, obj: Any) -> Any:
        parser = ParserManager._get_parser(ty)
        return parser.parse(ty, obj, self)

