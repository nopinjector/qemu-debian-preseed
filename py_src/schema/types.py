import typing
import inspect
from .transform import Transformation


class SchemaType:
    pass


class TypeList(SchemaType):
    _sub_ty = None

    @classmethod
    def get_sub_type(cls) -> typing.Type:
        return cls._sub_ty


class TypeUnion(SchemaType):
    _union_tys = []

    @classmethod
    def get_union_types(cls) -> typing.List[typing.Type]:
        return cls._union_tys


class TypeOptional(SchemaType):
    _sub_ty = None

    @classmethod
    def get_sub_type(cls) -> typing.Type:
        return cls._sub_ty


class TypeInteger(SchemaType):
    pass


class TypeString(SchemaType):
    pass


class TypeStructure(SchemaType):
    @classmethod
    def attributes(cls) -> typing.Iterable[str]:
        for attr_name in dir(cls):
            if attr_name.startswith('_'):
                continue

            attr = getattr(cls, attr_name)
            if inspect.isfunction(attr) or inspect.ismethod(attr):
                continue

            yield attr_name

    @classmethod
    def transformations(cls) -> typing.Iterable[typing.Type[Transformation]]:
        return []


def List(sub_ty: typing.Type) -> typing.Type:
    class _type(TypeList):
        _sub_ty = sub_ty

    return _type


def Union(*union_tys: typing.Type) -> typing.Type:
    class _type(TypeUnion):
         _union_tys = list(union_tys)

    return _type


def Optional(sub_ty: typing.Type) -> typing.Type:
    class _type(TypeOptional):
        _sub_ty = sub_ty

    return _type


def String() -> typing.Type:
    return TypeString


def Integer() -> typing.Type:
    return TypeInteger


Structure = TypeStructure
