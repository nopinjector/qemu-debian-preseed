class SchemaException(Exception):
    pass


class SchemaVerificationException(Exception):
    pass


class SchemaParserException(Exception):
    pass
