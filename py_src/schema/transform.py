from typing import Type, Any


class Transformation:
    @classmethod
    def transformable(cls, ty: Type, obj: Any) -> bool:
        raise NotImplementedError

    @classmethod
    def transform(cls, ty: Type, obj: Any) -> Any:
        raise NotImplementedError


class FreeFormTransformation(Transformation):
    @classmethod
    def transformable(cls, ty: Type, obj: Any) -> bool:
        # obj is already a dict: no transformation required
        if isinstance(obj, dict):
            return True

        return getattr(ty, '_free_field', None) is not None

    @classmethod
    def transform(cls, ty: Type, obj: Any) -> Any:
        # obj is already a dict: no transformation required
        if isinstance(obj, dict):
            return obj

        free_field_name = getattr(ty, '_free_field')

        return {
            free_field_name: obj
        }


class KeywordUnpackTransformation(Transformation):
    @staticmethod
    def _get_keyword(obj: Any) -> str:
        if not isinstance(obj, dict) or len(obj) != 1:
            raise Exception('not a valid command')

        return next(iter(obj))

    @classmethod
    def transformable(cls, ty: Type, obj: Any) -> bool:
        return KeywordUnpackTransformation._get_keyword(obj) == ty._keyword

    @classmethod
    def transform(cls, ty: Type, obj: Any) -> Any:
        return obj[KeywordUnpackTransformation._get_keyword(obj)]
