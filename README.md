# Qemu Debian Preseed

"Qemu Debian Preseed" utilises the functionalities of QEMU to bootstrap a Debian based distribution fully automated
into a virtual machine. This base functionality, which is completely delivered by QEMU is enriched with an easy
customizable templating environment for the virtual machines based on Jinja2 templates.

# Prerequisites

Currently, it is only tested under Debian (but it may also work under other distributions).

Dependencies (for bootstrapping):
* qemu-system-x86
* qemu-utils
* python3
* python3-yaml
* python3-jinja2

Dependencies (for downloading of kernel):
* wget
* gpg

# Quick Start

To bootstrap your first VM, just run

```shell script
# Create (empty) virtual disk
qemu-img create -f qcow2 -o preallocation=metadata vmdisk.qcow2 10G
# Download and verify kernel and initramfs for bootstrapping
./prepare.sh debian-stable
# Run automatic installation
./bootstrap.py --drive vmdisk.qcow2 --fqdn debianvm.virtual.local --preseed preseed/default-lvm.jinja2 --options options/debian-stable-en.yml
```
The root password for the VM is automatically generated and printed on console.

If desired, the created VM can be enriched with several features, like a vagrant setup or development utils. Just use
```shell script
./bootstrap.py --drive disk.img --fqdn debianvm.virtual.local --preseed preseed/default-lvm.jinja2 --options options/debian-stable-en.yml --feature features/vagrant-public.yml --feature features/console-utils.yml --feature features/build-environment.yml
```

# How it works
QEMU Debian Preseed uses the direct kernel boot functionality from QEMU to boot the (Debian) netboot kernel
and initramfs. Using preseed files delivered over the QEMU built-in TFTP server the installation runs fully automated.

The kernel and initramfs images are downloaded from the distribution repositories and verified by using the (running)
systems APT PGP key. This is done by `prepare.sh` and uses the configuration inside the `prepare_conf` folder.

The preseed configuration required for running the automated installation is generated from Jinja2 based preseed
templates (see `preseed/default-lvm.jinja2` for example) which is enriched by a YAML options file (loaded as Jinja2
variables in the template). Both, *preseed* and *option* file can be specified on bootstrapping.

Furthermore, it is possible to add additional *features* to the VM (like tools for build environment). These are executed
at the end of the Debian installation process and enable the user to copy certain files into VM or execute commands in
VMs environment (a little bit as Dockerfile's).

# Vagrant base box generation
One motive for this project was having an easy Vagrant base box generation utility for running Vagrant VMs on libvirt
backend. Therefore, this project also contains some specific Vagrant configuration and helpers.

`scripts/create-libvirt-vagrant-box.sh` fully automates the creation of a new Vagrant base box for use with libvirt.
```shell script
./scripts/create-libvirt-vagrant-box.sh --size 30 --output .boxes/vagrant_debian.box --box scripts/vagrant-boxes/devbox
```
`scripts/update-box-catalog-file.py` creates a Vagrant catalog file for versioning of the base box.
```shell script
./scripts/update-box-catalog-file.py --name awesome-vagrant-box --box-dir .boxes/
```

A short shell script will aid to combine both utilities into a single tool:
```shell script
#!/bin/sh -e
SCRIPT_DIR="$(dirname "$(readlink -f "$0")")"
BASE_PATH=<path to copy of this repository>/scripts/
BOX_SIZE=30
TIMESTAMP=$(date '+%Y%m%d.%H%M')

mkdir -p "${SCRIPT_DIR}/box-img"
${BASE_PATH}/create-libvirt-vagrant-box.sh --size "${BOX_SIZE}" --output "${SCRIPT_DIR}/box-img/${TIMESTAMP}.box" --box ${BASE_PATH}/vagrant-boxes/devbox
${BASE_PATH}/update-box-catalog-file.py --name dev-basebox --box-dir "${SCRIPT_DIR}/box-img" > "${SCRIPT_DIR}/box.json"
```

The Vagrantfile may look like this:
```text
Vagrant.configure("2") do |config|
  config.vm.box = "dev-basebox"
  config.vm.box_url = "file:///vagrant/dev-basebox/box.json"
end
```