#!/bin/bash -e

SCRIPT_DIR="$(dirname "$(readlink -f "$0")")"

function check_prerequisites {
  command -v guestfish >/dev/null 2>&1 || (echo "guestfish required, but not found! exiting..."; exit 127)
}

function print_usage_and_exit {
  echo "usage: $0 --disk <disk image> [--tar <tarfile> | --docker <img-name> ]"
  exit 1
}

function parse_arguments {
  while [[ $# -ge 2 ]]
  do
    case $1 in
        --tar)
          TARFILE="$2"
        ;;
        --docker)
          DOCKER_IMG="$2"
        ;;
        --disk)
          DISK_PATH="$2"
        ;;
        *)
          print_usage_and_exit
        ;;
    esac
    shift
    shift
  done

  if [[ -z ${TARFILE+x} && -z ${DOCKER_IMG+x} ]]; then
    print_usage_and_exit
  elif [[ -z ${DISK_PATH+x} ]]; then
    print_usage_and_exit
  fi
}

function extract_from_disk {
  local disk_path="$1"

  guestfish \
      --ro \
      --add "${disk_path}" \
      --inspector \
      tar-out / - numericowner:true xattrs:true selinux:true acls:true
}

check_prerequisites
parse_arguments "$@"

if [[ -n "${TARFILE+x}" ]]; then
  extract_from_disk "${DISK_PATH}" | gzip > "${TARFILE}"
elif [[ -n "${DOCKER_IMG+x}" ]]; then
  extract_from_disk "${DISK_PATH}" | docker import - "${DOCKER_IMG}"
fi