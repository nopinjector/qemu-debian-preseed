#!/usr/bin/python3
import os
import json
import tarfile
import hashlib
from argparse import ArgumentParser
from typing import List, Dict

# Read here:
#   https://github.com/hollodotme/Helpers/blob/master/Tutorials/vagrant/self-hosted-vagrant-boxes-with-versioning.md


def calculate_filehash_sha1(path: str) -> str:
    CHUNK_SIZE = 1024**2
    sha1 = hashlib.sha1()
    with open(path, 'rb') as fd:
        chunk = fd.read(CHUNK_SIZE)
        while chunk != b'':
            sha1.update(chunk)
            chunk = fd.read(CHUNK_SIZE)

    return sha1.hexdigest()


def read_box_version(path: str) -> str:
    with tarfile.open(path, 'r') as file:
        info_file = file.extractfile('info.json')
        info_json = info_file.read()
        info_obj = json.loads(info_json.decode('ascii'))

        return info_obj['version']


def generate_version_entry(path: str) -> Dict[str, str]:
    hashsum = calculate_filehash_sha1(path)
    version = read_box_version(path)
    abs_path = os.path.abspath(path)

    return {
        'version': version,
        'providers': [{
            'name': 'libvirt',
            'url': 'file://{:s}'.format(abs_path),
            'checksum_type': 'sha1',
            'checksum': hashsum
        }]
    }


def list_box_versions(path: str) -> List[Dict[str, str]]:
    versions = []

    for root, dirs, files in os.walk(path):
        for file in files:
            if not file.endswith('.box'):
                continue

            full_path = os.path.join(root, file)
            entry = generate_version_entry(full_path)
            versions.append(entry)

    return versions


def generate_catalog_file(box_name: str, versions: List[Dict[str, str]]) -> bytes:
    catalog = {
        'name': box_name,
        'description': '',
        'versions': versions
    }

    return json.dumps(catalog, indent=4)


def main():
    parser = ArgumentParser()
    parser.add_argument('--name', action='store', required=True)
    parser.add_argument('--box-dir', action='store', dest='box_dir', required=True)

    args = parser.parse_args()

    versions = list_box_versions(args.box_dir)
    cat_file = generate_catalog_file(args.name, versions)

    print(cat_file)


if __name__ == '__main__':
    main()
