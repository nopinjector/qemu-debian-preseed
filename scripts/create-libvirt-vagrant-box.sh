#!/bin/bash
set -e

SCRIPT_DIR="$(dirname "$(readlink -f "$0")")"
ROOT_DIR=$(realpath "${SCRIPT_DIR}/..")

function check_prerequisites {
  command -v gzip >/dev/null 2>&1 || (echo "gzip required, but not found. exiting!"; exit 127)
  command -v tar >/dev/null 2>&1 || (echo "tar required, but not found. exiting!"; exit 127)
  command -v qemu-img >/dev/null 2>&1 || (echo "qemu-img required, but not found. exiting!"; exit 127)
}

function print_usage_and_exit {
  echo "usage: $0 --size <img-size> --output <dest-file>"
  exit 1
}

function build_feature_list_from_boxconfig {
  local box="$1"
  local boxconfig=$(cat "${box}")
  local featureline=""

  while read -r feature; do
    if [ -f "${ROOT_DIR}/features/${feature}.yml" ]; then
      featureline="${featureline} --feature ${ROOT_DIR}/features/${feature}.yml"
    elif [ -f "${feature}.yml" ]; then
      featureline="${featureline} --feature ${feature}.yml"
    else
      echo "feature from box configuration does not exist! exiting..."
      exit 1;
    fi
  done <<< "${boxconfig}"

  echo "${featureline}"
}

function parse_arguments {
  while [[ $# -ge 2 ]]
  do
    case $1 in
        -s|--size)
          IMG_SIZE="$2"
        ;;
        -o|--output)
          OUTFILE="$2"
        ;;
        -b|--box)
          FEATURES=$(build_feature_list_from_boxconfig "$2")
        ;;
        --feature)
          FEATURES="${FEATURES} --feature $2"
        ;;
        *)
          print_usage_and_exit
        ;;
    esac
    shift
    shift
  done

  if [[ -z ${IMG_SIZE+x} || -z ${OUTFILE+x} ]]; then
    print_usage_and_exit
  fi
}

function bootstrap_box {
  local size=$1
  local target="$2"
  local features="$3"

  local TMP_DIR=$(mktemp -d)
  trap "rm -rf ${TMP_DIR}" RETURN INT TERM EXIT

  qemu-img create -f qcow2 -o preallocation=metadata "${TMP_DIR}/box.img" "${size}G"

  ${ROOT_DIR}/prepare.sh debian-stable
  ${ROOT_DIR}/bootstrap.py \
    --drive "${TMP_DIR}/box.img" \
    --fqdn "vagrant.virtual.local" \
    --preseed "${ROOT_DIR}/preseed/default-lvm.jinja2" \
    --options "${ROOT_DIR}/options/debian-stable-de.yml" \
    ${features} \
    --feature "${ROOT_DIR}/features/vagrant-public.yml"

    # Cleanup / Compress the image
    qemu-img convert "${TMP_DIR}/box.img" -O qcow2 "${target}"
}

function pack_box {
  local tmpdir="$1"
  local size="$2"
  local outfile="$3"

  local version=$(date '+%Y%m%d.%H%M')

  cat > "${tmpdir}/metadata.json" <<EOF
{
    "provider": "libvirt",
    "format": "qcow2",
    "virtual_size": ${size}
}
EOF

  cat > "${tmpdir}/info.json" <<EOF
{
    "version": "${version}"
}
EOF

  cat > "${tmpdir}/Vagrantfile" <<EOF
Vagrant.configure("2") do |config|
  config.vm.provider :libvirt do |libvirt|
    libvirt.driver = "kvm"
    libvirt.host = ""
    libvirt.connect_via_ssh = false
    libvirt.storage_pool_name = "default"
  end
  config.vm.synced_folder ".", "/vagrant", disabled: true
end
EOF

  tar cv -S --totals -C "${tmpdir}" "metadata.json" "info.json" "Vagrantfile" "box.img" 2>/dev/null | gzip -c > "${outfile}"
}


check_prerequisites
parse_arguments "$@"

TMP_DIR=$(mktemp -d)
trap "rm -rf ${TMP_DIR}" INT TERM EXIT

bootstrap_box "${IMG_SIZE}" "${TMP_DIR}/box.img" "${FEATURES}"
pack_box "${TMP_DIR}" "${IMG_SIZE}" "${OUTFILE}"
