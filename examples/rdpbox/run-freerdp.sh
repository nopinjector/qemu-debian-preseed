#!/bin/bash -e

SCRIPT_DIR="$(dirname "$(readlink -f "$0")")"
PWD=$(realpath "$(pwd)")

function determine_ip {
  local vm_name="$1"

  local VIRSH="virsh -c qemu:///system"

  # Read ip address from virtual machine using virsh
  #
  # The parsing of the following table
  #
  #         Name       MAC address          Protocol     Address
  #        -------------------------------------------------------------------------------
  #         vnet0      52:54:00:9c:fc:49    ipv4         192.168.121.167/24
  #         vnet1      52:54:00:01:95:43    ipv4         172.28.128.27/24
  #
  # is done using this ugly expression:
  #
  # $VIRSH domifaddr "${vm_name}" \
  #      | tail -n +3 \         # cut off the header
  #      | grep ipv4 \          # only ipv4 addresses
  #      | sed -E 's/ +/ /g' \  # multiple whitespaces with one (for cut command)
  #      | cut -d ' ' -f 5 \    # extract field 5 - the ip address
  #      | head -1 \            # we only need one ip if we have multiple
  #      | sed -E 's/\/.*$//'   # cut off the subnet

  $VIRSH domifaddr "${vm_name}" \
      | tail -n +3 \
      | grep ipv4 \
      | sed -E 's/ +/ /g' \
      | cut -d ' ' -f 5 \
      | head -1 \
      | sed -E 's/\/.*$//'
}

function get_vm_name {
  echo "$(basename "${PWD}")_default"
}

function start_vm {
  STATUS=$( (cd "${PWD}" && vagrant status --machine-readable) | grep ',state,')
  if [[ "${STATUS}" =~ .*shutoff.* || "${STATUS}" =~ .*not_created.* ]]; then
    (cd "${PWD}" && vagrant up)
    STARTED_VM=1
  fi
}

function stop_vm {
  if [[ "${STARTED_VM}" -ne 0 ]]; then
    (cd "${PWD}" && vagrant halt)
  fi
}

STARTED_VM=0

start_vm
trap stop_vm INT TERM EXIT
IP_ADDR=$(determine_ip "$(get_vm_name)")

echo "Trying to connect to ip '${IP_ADDR}' via RDP..."
xfreerdp +glyph-cache -grab-keyboard -clipboard -toggle-fullscreen \
    /size:100% /network:lan /dynamic-resolution /cert-ignore /u:vagrant /p:vagrant /title:"vagrant Remote GUI" "/v:${IP_ADDR}" 2>&1 >/dev/null

