MIRROR="http://http.kali.org/kali"
RELEASE=kali-rolling
ARCH=amd64
DIST_NAME=kali


function installer_get_signatures_dist {
  local mirror=$1
  local release=$2
  local arch=$3
  local destdir=$4
  local signdir="${destdir}/${SIGNATURES_DIR}"

  wget -q "${mirror}/dists/${release}/main/installer-${arch}/current/images/netboot/SHA256SUMS" -O "${signdir}/SHA256SUMS"
  wget -q "${mirror}/dists/${release}/main/installer-${arch}/current/images/netboot/SHA256SUMS.gpg" -O "${signdir}/SHA256SUMS.gpg"
}

function verify_file_signature_dist {
  local signdir=$1
  local file=$2
  local suburl=$3

  hash_exists_in_sha256_file "${signdir}" "${file}" "${suburl/netboot\//}"
  verify_apt_pgp_signature "${signdir}/SHA256SUMS" "${signdir}/SHA256SUMS.gpg"
}

function extract_from_netboot_tarfile {
  local netboot=$1
  local destdir=$2

  local unpackdir=$(mktemp -d)
  trap "rm -rf ${unpackdir}" RETURN INT TERM EXIT

  tar xzf "${netboot}" "-C${unpackdir}"
  cp "${unpackdir}/debian-installer/amd64/linux" "${destdir}/linux"
  cp "${unpackdir}/debian-installer/amd64/initrd.gz" "${destdir}/initrd.gz"
}

# Kali Linux does not have signatures for kernel and initramfs, but it has a signature for
# a tar archive containing them. Therefore, we download this archive and extract kernel
# and initramfs from there.
function get_installer_modules {
  local mirror=$1
  local release=$2
  local arch=$3
  local basedir=$4

  mkdir -p "${basedir}"

  installer_get_signatures "${mirror}" "${release}" "${arch}" "${basedir}"

  installer_verified_get "${mirror}" "${release}" "${arch}" "${basedir}" \
    "${basedir}/netboot.tar.gz" "netboot/netboot.tar.gz"

  echo "Unpacking netboot.tar.gz..."
  extract_from_netboot_tarfile "${basedir}/netboot.tar.gz" "${basedir}"

  echo "Decompressing initramfs..."
  gunzip -f "${basedir}/initrd.gz"
}
