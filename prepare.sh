#!/bin/bash
set -e

SCRIPT_DIR="$(dirname "$(readlink -f "$0")")"

# global constants
SIGNATURES_DIR=".signatures"
DOWNLOAD_DIR=".netboot-installer"

function check_prerequisites {
  command -v wget >/dev/null 2>&1 || (echo "wget required, but not found! exiting..."; exit 127)
  command -v gpg >/dev/null 2>&1 || (echo "gpg required, but not found! exiting..."; exit 127)
  command -v sha256sum >/dev/null 2>&1 || (echo "sha256sum required, but not found! exiting..."; exit 127)

  [ -f "${SCRIPT_DIR}/prepare_conf/apt_keys/${DIST_NAME}-archive-keyring.gpg" ] || \
    (echo "APT keyring for '${DIST_NAME}' not found. exiting..."; exit 127)
}

function installer_get_signatures {
  local mirror=$1
  local release=$2
  local arch=$3
  local destdir=$4

  local signdir="${destdir}/${SIGNATURES_DIR}"

  mkdir -p "${signdir}"

  echo "Getting release and hashsum files for integrity verification..."

  # signature files are distribution depedent
  installer_get_signatures_dist "${mirror}" "${release}" "${arch}" "${basedir}"
}

function verify_apt_pgp_signature {
  local file=$1
  local signfile=$2

  # No trust check required, it is enough that the signature key is contained in the keyring
  gpg --no-default-keyring \
      --keyring "${SCRIPT_DIR}/prepare_conf/apt_keys/${DIST_NAME}-archive-keyring.gpg" \
      --trust-model always \
      --quiet \
      --verify "${signfile}" "${file}" > /dev/null 2>&1
}

function hash_exists_in_sha256_file {
  local signdir=$1
  local file=$2
  local suburl=$3

  SHA_HASH_DOWNLOAD=$(sha256sum "${file}" | cut -d ' ' -f 1)
  SHA_HASH_IN_SHAFILE=$(cat "${signdir}/SHA256SUMS" | grep -F "${suburl}" | cut -d ' ' -f 1)

  if [ "${SHA_HASH_DOWNLOAD}" != "${SHA_HASH_IN_SHAFILE}" ]; then
    echo "SHA256 hash sums does not match. corrupt downloaded file! exiting..."
    exit 1
  fi
}

function verify_sha256_integrity {
  local signdir=$1

  SHA_HASH_SHAFILE=$(sha256sum ${signdir}/SHA256SUMS | cut -d ' ' -f 1)
  SHA_HASH_IN_RELASE_FILE=$(cat ${signdir}/Release | sed '1,/^SHA256:$/d' | grep -F "installer-${arch}/current/images/SHA256SUMS" | cut -d ' ' -f 2)

  if [ "${SHA_HASH_SHAFILE}" != "${SHA_HASH_IN_RELASE_FILE}" ]; then
    echo "Hashsum in Release file not found or does not match. corrupt SHA256SUMS file! exiting..."
    exit 1
  fi
}

function verify_file_signature {
  local signdir=$1
  local file=$2
  local suburl=$3

  # signature verification is distribution depedent
  verify_file_signature_dist "${signdir}" "${file}" "${suburl}"
}

function installer_verified_get {
  local mirror=$1
  local release=$2
  local arch=$3
  local basedir=$4
  local targetfile=$5
  local suburl=$6

  local signdir="${basedir}/${SIGNATURES_DIR}"

  echo "Downloading file '${suburl}'..."
  wget -q --show-progress "${mirror}/dists/${release}/main/installer-${arch}/current/images/${suburl}" -O "${targetfile}"

  echo "Verifying integrity of file '${suburl}'..."
  verify_file_signature "${signdir}" "${targetfile}" "${suburl}"
}

function get_installer_modules {
  local mirror=$1
  local release=$2
  local arch=$3
  local basedir=$4

  mkdir -p "${basedir}"

  installer_get_signatures "${mirror}" "${release}" "${arch}" "${basedir}"

  installer_verified_get "${mirror}" "${release}" "${arch}" "${basedir}" \
    "${basedir}/linux" "netboot/${DIST_NAME}-installer/amd64/linux"
  installer_verified_get "${mirror}" "${release}" "${arch}" "${basedir}" \
    "${basedir}/initrd.gz" "netboot/${DIST_NAME}-installer/amd64/initrd.gz"

  echo "Decompressing initramfs..."
  gunzip -f "${basedir}/initrd.gz"
}

if [ "$#" -ne 1 ]; then
  echo "usage: $0 <prepare-conf>"
  exit 1
fi

if [ ! -f "${SCRIPT_DIR}/prepare_conf/$1" ]; then
  echo "your specified prepare configuration can not be found! exiting..."
  exit 1
fi

. "${SCRIPT_DIR}/prepare_conf/$1"

check_prerequisites
get_installer_modules "${MIRROR}" "${RELEASE}" "${ARCH}" "${DOWNLOAD_DIR}"

echo "Finish!"