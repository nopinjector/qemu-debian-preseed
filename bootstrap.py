#!/usr/bin/python3
import os
import random
import yaml
import jinja2
import tempfile
import subprocess
import argparse
import crypt
import hashlib
from py_src.features import FeatureFile
from typing import Optional, List


def load_template(path: str) -> jinja2.Template:
    with open(path, 'r') as fd:
        return jinja2.Template(fd.read())


def setup_tmpdir(tmpdir: str):
    os.link(os.path.join(os.getcwd(), '.netboot-installer', 'linux'), os.path.join(tmpdir, 'linux'))
    os.link(os.path.join(os.getcwd(), '.netboot-installer', 'initrd'), os.path.join(tmpdir, 'initrd'))
    os.mkdir(os.path.join(tmpdir, '.tftp'))


def calc_md5sum(path: str):
    md5 = hashlib.md5()
    with open(path, 'rb') as fd:
        chunk = fd.read(2 ** 10)
        while chunk != b'':
            md5.update(chunk)
            chunk = fd.read(2 ** 10)

    return md5.hexdigest()


def random_password(length: int):
    pw = ''
    rand = random.SystemRandom()
    for i in range(length):
        pw += rand.choice('abcdefghijklmnopqrstuvwyxzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789')
    return pw


def run_qemu(drive: str, tmpdir: str):
    additional_args = []
    if os.getuid() == 0:
        # drop privileges:
        additional_args += [
            '-runas', 'nobody',
            '-sandbox', 'on',
            # TODO: chroot
        ]

    subprocess.run(args=[
        '/usr/bin/qemu-system-x86_64',
        '-enable-kvm',
        '-machine', 'pc,accel=kvm',
        '-m', '1024',
        '-curses',
        '-device', 'virtio-net-pci,netdev=net00',
        '-netdev', 'type=user,id=net00,tftp={:s}/.tftp,net=172.16.0.0/24'.format(tmpdir),
        '-drive', 'if=virtio,file={:s},cache=unsafe'.format(drive),
        '-no-reboot',
        '-kernel', '{:s}/linux'.format(tmpdir),
        '-initrd', '{:s}/initrd'.format(tmpdir),
        '-append', ' '.join([
            'keyboard-configuration/xkb-keymap=de',
            'nomodeset',
            'fb=false',
            'priority=critical',
            'locale=en_US',
            'preseed/url=tftp://172.16.0.2/preseed.cfg',
            'preseed/url/checksum={:s}'.format(calc_md5sum(os.path.join(tmpdir, '.tftp', 'preseed.cfg')))
        ]), *additional_args
    ], check=True)


def separate_fqdn(fqdn: str):
    parts = fqdn.split('.')
    return parts[0], '.'.join(parts[1:])


def load_options_file(path: str):
    with open(path, 'r') as fd:
        return yaml.safe_load(fd)


def create_preseed(preseed: str, options: str, dest_dir: str, fqdn: str, features: Optional[List[str]]):
    hostname, domain = separate_fqdn(fqdn)
    root_pw = random_password(16)
    parameters = load_options_file(options)

    for feature_path in features:
        feature = FeatureFile(feature_path)
        feature.create_tftp_content(dest_dir)
        parameters['install']['commands'] += feature.get_preseed_commands()
        parameters['install']['packages'] += feature.get_preseed_packages()

    env = jinja2.Environment(loader=jinja2.FileSystemLoader(os.path.dirname(preseed)))
    template = env.get_template(os.path.basename(preseed))
    result = template.render({
        'hostname': hostname,
        'domain': domain,
        'root_pw': crypt.crypt(root_pw, crypt.METHOD_SHA512),
        **parameters
    })
    preseed_dest = os.path.join(dest_dir, 'preseed.cfg')
    with open(preseed_dest, 'w') as fd:
        fd.write(result)

    print('Your root password is \'{:s}\''.format(root_pw))


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--drive',
        action='store',
        type=str,
        required=True,
        help='path to volume or virtual disk where system should be installed'
    )
    parser.add_argument(
        '--fqdn',
        action='store',
        type=str,
        required=True,
        help='FQDN of the system'
    )
    parser.add_argument(
        '--preseed',
        action='store',
        type=str,
        required=True,
        help='path to debian installer preseed file'
    )
    parser.add_argument(
        '--options',
        action='store',
        type=str,
        required=True,
        help='path to preseed parametrization file'
    )
    parser.add_argument(
        '--feature',
        dest='features',
        action='append',
        type=str,
        default=[],
        required=False,
        help='path to feature file'
    )

    args = parser.parse_args()

    with tempfile.TemporaryDirectory() as tmpdir:
        setup_tmpdir(tmpdir)
        create_preseed(args.preseed, args.options, os.path.join(tmpdir, '.tftp'), args.fqdn, args.features)
        run_qemu(args.drive, tmpdir)


if __name__ == '__main__':
    main()
